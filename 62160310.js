const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
var mysql = require('mysql');
const { NULL } = require('mysql/lib/protocol/constants/types');

let server = express();

server.use(bodyParser.json()); // ให้ server(express) ใช้งานการ parse json
server.use(morgan('dev')); // ให้ server(express) ใช้งานการ morgam module
server.use(cors()); // ให้ server(express) ใช้งานการ cors module
server.use(bodyParser.urlencoded({ extended: true }))// แปลงข้อมูลที่ส่งมาจากฟอร์ม
server.use(express.static('html'));
server.use(express.static('image'));


server.set('views', __dirname + '/public');// ระบุตำแหน่งของไฟล์ html
server.engine('html', require('ejs').renderFile);

var status = false;


let user = {}

const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'dcoffee',
    password: '',
});

con.connect((err) => {
    if (err) {
        console.log('Error connecting to Db');
        return;
    }
    console.log('Connection established');
});

//get login display
server.get('/logout', function (req, res) {
    //get display logout
    console.log('user logout succeed')
    const id = user.data.id_employee
    con.query('INSERT INTO trn_logout (id_employee, datetime_logout) VALUES (?, NOW())', id, function (err) {
        if (err) return err;
    })
    // res.render(__dirname + "/public/login.html")
    res.redirect('/')
});

//form input user to data
server.post('/login', function (req, res) {
    //user display form login
    console.log('user login display')
    let email = req.body.email;
    let password = req.body.password;
    console.log('input email = ' + email + ' ' + password)
    con.query('SELECT * FROM mst_security WHERE user = ?', [email], function (err, data) {
        if (err) return err;
        if (data[0] == null) {
            //input form email && password == null
            console.log('datainput = ' + data)
            return res.sendFile(__dirname + '/public/nonmember.html')
        }
        if (password != data[0].password) {
            //input email != null && password == null 
            console.log("user not input password");
            return res.sendFile(__dirname + '/public/nopassword.html')
        }
        //SELECT data user
        con.query('SELECT * FROM mst_employee WHERE id_employee = ?', [data[0].id_employee], function (err, data2) {
            console.log('data 2 = ' + data2);
            user = {
                data: data2[0],
                isCRUD: "true"
            }
            //display to main succeed 
            res.redirect('/main')
        })
    })
});

//get main display
server.get('/main', function (req, res) {
    const id = user.data.id_employee
    //login in
    con.query('INSERT INTO trn_login (id_employee, detetime_login) VALUES (?, NOW())', id, function (err) {
        if (err) return err;
    })

    //data all to html
    con.query('SELECT * FROM `mst_employee` ORDER BY id_employee asc', (err, rows) => {
        if (err) return err;
        if (status) {
            res.render(__dirname + "/public/main_menus.html", {
                data: rows,
                name: user.data.name,
                surname: user.data.surname,
                position: user.data.position,
                isCRUD: "true"
            })
        } else {
            res.render(__dirname + "/public/main_menus.html", {
                data: rows,
                name: user.data.name,
                surname: user.data.surname,
                position: user.data.position,
                isCRUD: "false"

            })
        }
        console.log('ID Login' + user.data.id_employee + ' ' + user.data.name + ' ' + user.data.surname + ' ' + user.data.position)
        console.log('-------------------------')
        for (var i = 0; i < rows.length; i++) {
            console.log('rows ' + rows[i].id_employee)
            console.log('rows ' + rows[i].name)
            console.log('rows ' + rows[i].surname)
            console.log('rows ' + rows[i].position)
            console.log('rows ' + rows[i].salary)
            console.log('rows ' + rows[i].total_sale)
            console.log('-------------------------')
        }
    })
})

server.get('/delete/:userId', function (req, res) {
    const userId = req.params.userId;
    console.log("page confirm delete = " + userId)
    con.query('SELECT * FROM mst_employee WHERE id_employee = ' + userId, function (err, rows) {
        if (err) return err;
        data2 = rows[0]            
        console.log("Data delete to page delete -> name = "+data2.name+" surnname = "+data2.surname+" position = "+data2.position+" salary = "+data2.salary+" total_sale = "+data2.total_sale)
    })
    
    con.query('SELECT * FROM mst_security WHERE id_employee = ' + userId, function (err, rows) {
        if (err) return err;
        data = rows[0]
        console.log("data -> user = "+data.user+" password = "+data.password)
        res.render(__dirname + "/public/confirm_delete.html", {
            data, data2           
        })
        
        console.log("data to delete")
    })
})


//get delete 1 user = delete 1 employee and 1 mst_security
server.post('/delete/:userId', (req, res) => {
    const userId = req.body.deltid;
    status = true;
    console.log('userId post delete = ' + userId);
    //logout FK
    con.query("DELETE FROM `trn_logout` WHERE id_employee =" + userId, (err, result) => {
        if (err) return err;
        console.log("Delete from logout")
    })
    //login FK
    con.query("DELETE FROM `trn_login` WHERE id_employee =" + userId, (err, result) => {
        if (err) return err;
        console.log("Delete from login")
    })
    //security FK
    con.query("DELETE FROM `mst_security` WHERE id_employee =" + userId, (err, result) => {
        if (err) return err;
        //ต้องลบตัวที่ลิงค์ PK ก่อน คือ FK แล้วถึงจะสามารถลบ PK ได้
        console.log("Delete 1 mst_security defore delete 1 employee (delete FK defore PK)")
    })
    //employee PK
    con.query("DELETE FROM `mst_employee` WHERE id_employee =" + userId, (err, result) => {
        // if (err) throw err;
        if (err) return err;
        res.redirect('/main');
    });
});

//get to display form add new user
server.get('/add', function (req, res) {
    console.log("position page get add")
    console.log('-------------------------')
    res.render(__dirname + "/public/add.html", {
        name: '',
        isCRUD: "true"
    })
})

//form add new user
server.post('/add', function (req, res) {
    console.log("TEST post input form")
    status = true;
    let data = { name: req.body.addname, surname: req.body.addsurname, position: req.body.addposition, salary: req.body.addsalary, total_sale: req.body.addtotal_sale };
    console.log("INPUT DATA =" + data)
    let sql = "INSERT INTO mst_employee SET ?";
    let query = con.query(sql, data, (err, results) => {
        insert = results.insertId
        if (err) throw err;
        var security = {
            user: req.body.addemail,
            password: req.body.addpassword,
            id_employee: results.insertId
        }
        con.query('INSERT INTO mst_security SET ?', security, (err, res3) => {
            if (err) return err;
            console.log('Insert last ID = ' + res3.insertId)
        })
        res.redirect('/main');
    });
    console.log("Insert succeed")
});

//get to display form edit 1 user
server.get('/edit/:userId', function (req, res) {
    var user = "";
    var pass = "";
    var id = "";
    console.log("get Edit")
    const userId = req.params.userId;
    console.log('userId = ' + userId);
    //Edit Email && password form mst security
    con.query('SELECT * FROM mst_security WHERE id_employee = ' + userId, function (err, rows) {
        if (err) return err;
        user = rows[0].user
        pass = rows[0].password
        id = rows[0].id_employee
        console.log("user = " + user)
    })
    //Edit data user form mst employee
    con.query('SELECT * FROM mst_employee WHERE id_employee = ' + userId, function (err, rows) {
        if (err) return err;
        res.render(__dirname + "/public/edit.html", {
            data: rows[0],
            id: rows[0].id_employee,
            user,
            pass,
            isCRUD: "true"
        })
    })
});

//update data 
server.post('/update', function (req, res) {
    console.log("page post update")
    status = true;
    const id = req.body.editid;
    console.log("id = " + id)
    //update email && password form mst security
    con.query("update mst_security SET user='" + req.body.edit_email + "', password='" + req.body.edit_password + "' where id_employee =" + id, (err, result) => {
        if (err) return err;
        console.log("update mst_security user && password")
    })
    //update all data user form mst employee
    con.query("update mst_employee SET name='" + req.body.editname + "',  surname='" + req.body.editsurname + "',  position='" + req.body.editposition + "',  salary='" + req.body.editsalary + "',  total_sale='" + req.body.edittotal_sale + "' where id_employee =" + id, (err, result) => {
        if (err) throw err;
        console.log("update mst_employee all data")
        //update successfully to main page
        res.redirect('/main');
    })
});

//search data form input to show crud
server.post('/search', function (req, res) {
    console.log("SEARCH");
    status = true;
    var search = req.body.search;
    if (search == "") {
        res.redirect('/main');
    } else {
        con.query('SELECT * FROM mst_employee WHERE name LIKE ? OR surname LIKE ?', ['%' + search + '%', '%' + search + '%'], (err, rows) => {
            if (err) return err;
            res.render(__dirname + "/public/main_menus.html", {
                data: rows,
                name: user.data.name,
                surname: user.data.surname,
                position: user.data.position,
                isCRUD: "true"
            });
        })
    }
})

//get to start display localhost
server.get('/', function (req, res) {
    res.sendFile(__dirname + "/public/" + "login.html");
});


server.listen(3000, function () {
    console.log('Server Listen at http://localhost:3000');
});
