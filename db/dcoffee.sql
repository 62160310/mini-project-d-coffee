-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2022 at 10:55 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dcoffee`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_employee`
--

CREATE TABLE `mst_employee` (
  `id_employee` int(11) NOT NULL,
  `name` char(255) DEFAULT NULL,
  `surname` char(255) DEFAULT NULL,
  `position` char(255) DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `total_sale` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_employee`
--

INSERT INTO `mst_employee` (`id_employee`, `name`, `surname`, `position`, `salary`, `total_sale`) VALUES
(1, 'สุขใจ', 'ไทยเดิม', 'พนักงาน', 5000, 30000),
(2, 'มานี', 'รักถิ่นไทย', 'เจ้าของร้าน', 6000, 50000),
(4, 'สิทธิชัย', 'ทองประเทือง', 'เจ้าของร้าน', 1.5693, 1000000.1234567);

-- --------------------------------------------------------

--
-- Table structure for table `mst_security`
--

CREATE TABLE `mst_security` (
  `id_security` int(11) NOT NULL,
  `user` char(255) DEFAULT NULL,
  `password` char(255) DEFAULT NULL,
  `id_employee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_security`
--

INSERT INTO `mst_security` (`id_security`, `user`, `password`, `id_employee`) VALUES
(1, 'sutgai.t@gmail.com', 'qw123', 1),
(2, 'manee.r@gmail.com', 'as123', 2),
(15, 'admin@go.buu.ac.th', 'admin123', 4);

-- --------------------------------------------------------

--
-- Table structure for table `trn_login`
--

CREATE TABLE `trn_login` (
  `id_login` int(11) NOT NULL,
  `detetime_login` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_employee` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_login`
--

INSERT INTO `trn_login` (`id_login`, `detetime_login`, `id_employee`) VALUES
(1, '2021-02-03 01:08:11', 1),
(2, '2021-02-03 05:08:11', 2),
(3, '2021-02-04 01:08:11', 1),
(4, '2021-02-04 01:08:11', 2),
(5, '2021-02-05 01:08:11', 2),
(6, '2021-02-06 01:08:11', 2),
(646, '2022-03-25 09:13:03', 2),
(647, '2022-03-25 09:13:25', 2),
(648, '2022-03-25 09:13:36', 2),
(649, '2022-03-25 09:54:37', 2),
(650, '2022-03-25 09:54:46', 2),
(651, '2022-03-25 09:54:51', 2),
(652, '2022-03-25 09:54:54', 2),
(653, '2022-03-25 09:55:15', 2),
(654, '2022-03-25 09:55:22', 2);

-- --------------------------------------------------------

--
-- Table structure for table `trn_logout`
--

CREATE TABLE `trn_logout` (
  `id_logout` int(11) NOT NULL,
  `datetime_logout` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_employee` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_logout`
--

INSERT INTO `trn_logout` (`id_logout`, `datetime_logout`, `id_employee`) VALUES
(1, '2021-02-03 09:08:11', 1),
(2, '2021-02-03 11:08:11', 2),
(3, '2021-02-04 09:08:11', 1),
(4, '2021-02-04 05:08:11', 2),
(5, '2021-02-05 09:08:11', 2),
(6, '2021-02-06 05:08:11', 2),
(154, '2022-03-25 09:35:43', 2),
(155, '2022-03-25 09:55:24', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_employee`
--
ALTER TABLE `mst_employee`
  ADD PRIMARY KEY (`id_employee`);

--
-- Indexes for table `mst_security`
--
ALTER TABLE `mst_security`
  ADD PRIMARY KEY (`id_security`),
  ADD KEY `id_employee` (`id_employee`);

--
-- Indexes for table `trn_login`
--
ALTER TABLE `trn_login`
  ADD PRIMARY KEY (`id_login`),
  ADD KEY `id_employee` (`id_employee`);

--
-- Indexes for table `trn_logout`
--
ALTER TABLE `trn_logout`
  ADD PRIMARY KEY (`id_logout`),
  ADD KEY `id_employee` (`id_employee`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_employee`
--
ALTER TABLE `mst_employee`
  MODIFY `id_employee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mst_security`
--
ALTER TABLE `mst_security`
  MODIFY `id_security` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `trn_login`
--
ALTER TABLE `trn_login`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=655;

--
-- AUTO_INCREMENT for table `trn_logout`
--
ALTER TABLE `trn_logout`
  MODIFY `id_logout` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mst_security`
--
ALTER TABLE `mst_security`
  ADD CONSTRAINT `mst_security_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `mst_employee` (`id_employee`),
  ADD CONSTRAINT `mst_security_ibfk_2` FOREIGN KEY (`id_employee`) REFERENCES `mst_employee` (`id_employee`);

--
-- Constraints for table `trn_login`
--
ALTER TABLE `trn_login`
  ADD CONSTRAINT `trn_login_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `mst_employee` (`id_employee`),
  ADD CONSTRAINT `trn_login_ibfk_2` FOREIGN KEY (`id_employee`) REFERENCES `mst_employee` (`id_employee`);

--
-- Constraints for table `trn_logout`
--
ALTER TABLE `trn_logout`
  ADD CONSTRAINT `trn_logout_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `mst_employee` (`id_employee`),
  ADD CONSTRAINT `trn_logout_ibfk_2` FOREIGN KEY (`id_employee`) REFERENCES `mst_employee` (`id_employee`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
